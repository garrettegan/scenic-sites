//
//  ContentView.swift
//  Scenic Sites
//
//  Created by Garrett Egan on 11/28/20.
//  Copyright © 2020 Garrett Egan. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
